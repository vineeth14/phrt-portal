/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable no-debugger, no-console */
import React from "react";
import Loader from "components/Loader";
import ScrollToTop from "components/scrollToTop";
import Toast from "components/Toast";
import AsideBar from "components/AsideBar";
import TopNavBar from "components/Header/index";
import Table from "components/ProjectListTable/Index";
import Pagination from "components/Pagination";
import { DashboardWrapper } from "./styled";
import { Footer } from "../../components/Footer/index";

const Dashboard = () => {
  /* const signin = () => {
    window.userManager
      .signinRedirect()
      .then((data: any) => {
        console.log("signinRedirect ok..................", data);
      })
      .catch((err: any) => {
        console.log("signinRedirect error:", err);
      });
  }; 
  */
  return (
    <DashboardWrapper className="container-fluid d-flex flex-column w-100 p-0">
      <main className="d-flex" id="maincontent">
        <div className="d-flex content-container">
          <AsideBar />
          <div className="left-container flex-grow-1">
            <TopNavBar />
            <ScrollToTop />
            <div className="">
              <div className="d-flex justify-content-between heading-bar">
                <h2 className="font-styled">Add Project</h2>
                <button
                  type="button"
                  className="btn btn-round btn-primary btn-height"
                >
                  Submit
                </button>
              </div>
              <div className="d-flex justify-content-between status-bar">
                <p className="d-flex" />
                Showing&#10240;
                <div className="dropdown">
                  <button
                    className="btn dropdown-toggle p-0"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-expanded="false"
                  >
                    1 - 25
                  </button>
                </div>
              </div>
              {/* <Pagination pageNumber=1 pageSize=5 totalCount=6 isFirst=true isLast=false /> */}
            </div>
          </div>
        </div>
      </main>
      <Footer />
      <Toast />
      <Loader />
    </DashboardWrapper>
  );
};

export default Dashboard;
