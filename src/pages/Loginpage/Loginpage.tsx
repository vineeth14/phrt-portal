/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable no-debugger, no-console */
import React from "react";
import Loader from "components/Loader";
import Toast from "components/Toast";
import ScrollToTop from "components/scrollToTop";
import { Wrapper } from "styled";
import { Link } from "react-router-dom";
import { Footer } from "../../components/Footer/index";
import { SignInWrapper } from "./styled";

const HomePage = () => {
  /* const signin = () => {
    window.userManager
      .signinRedirect()
      .then((data: any) => {
        console.log("signinRedirect ok..................", data);
      })
      .catch((err: any) => {
        console.log("signinRedirect error:", err);
      });
  }; 
  */
  return (
    <Wrapper>
      <SignInWrapper>
        <main>
          <ScrollToTop />
          <Toast />
          <div className="d-flex flex-column h-100">
            <div className="main-content d-flex flex-row flex-grow-1">
              <div className="w-100 image-section" id="maincontent">
                <img
                  className="mainpage-image"
                  src={`${process.env.PUBLIC_URL}/images/homepageMeeting.jpg`}
                  alt="Homepage"
                />
              </div>
              <div className="w-100 background-login">
                <div className="d-flex flex-column login-section flex-grow-1">
                  <img
                    className="LoginImage"
                    src={`${process.env.PUBLIC_URL}/images/PHRTLogin.png`}
                    alt="American Heart Association's Project Health Report Tool Logo"
                  />
                  <div className="d-flex signin-section">
                    <h1
                      className="h1"
                      style={{ color: "#555555", fontSize: "30px" }}
                    >
                      Sign In
                    </h1>
                    <p className="indicate-field">
                      <strong style={{ color: "#FF0000" }}>*</strong> Indicates
                      a required field
                    </p>
                  </div>
                  <form
                    className="form-group required flex-column"
                    action=""
                    method="post"
                  >
                    <label
                      className="col-sm-2 col-form-label label"
                      htmlFor="inputtext1"
                    >
                      Username/Email
                    </label>
                    <div className="input-border d-flex flex-row w-100">
                      <i className="mt-1 px-3 icons aha-icon-profile-android" />
                      <input
                        type="text"
                        className="form-control border-0 d-flex flex-grow-1"
                        placeholder="admin@heart.org"
                        id="inputtext1"
                        required
                      />
                    </div>
                    <label
                      className="col-sm-2 col-form-label label"
                      htmlFor="inputtext2"
                    >
                      Password
                    </label>
                    <div className="input-border d-flex flex-row w-100">
                      <div className="mt-1 px-3 icons">&#128274;</div>
                      <input
                        type="password"
                        className="form-control border-0 d-flex flex-grow-1"
                        placeholder="&#8727;&#8727;&#8727;&#8727;&#8727;&#8727;&#8727;&#8727;"
                        id="inputtext2"
                        required
                      />
                    </div>
                    <Link to="/dashboard">
                      <button
                        type="submit"
                        className="btn btn-round btn-primary submit-button"
                      >
                        Submit
                      </button>
                    </Link>
                  </form>
                </div>
              </div>
            </div>
            <Footer />
          </div>
          <Loader />
        </main>
      </SignInWrapper>
    </Wrapper>
  );
};

export default HomePage;
