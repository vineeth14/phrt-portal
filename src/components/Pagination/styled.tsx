import styled from "styled-components";

const PaginationStyle = styled.div`
  .page-disabled {
    pointer-events: none;
    opacity: 0.5;
  }
`;
export default PaginationStyle;
