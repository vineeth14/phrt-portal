/* eslint-disable prettier/prettier */
import React from "react";
import { Table } from "./styled";

function Tables() {
    return (
        <Table>
            <table className="aui-responsive-status-table table">
                <thead className="visuallyhidden">
                    <tr>
                        <th scope="col">Project Name</th>
                        <th scope="col">Scope</th>
                        <th scope="col">Quality</th>
                        <th scope="col">Schedule</th>
                        <th scope="col">Budget</th>
                        <th scope="col">Overall Health</th>
                    </tr>

                </thead>
                <tbody>
                    <tr className="aui-table-status-red">
                        <td className="title" data-title="Project Title">
                            <div className="aui-td justify-align">
                                <a href="/projectlist/projectname">Shop CPR</a>
                            </div>
                        </td>
                        <td data-title="Scope">
                            <div className="aui-td flex-column align-items-start justify-content-center">
                                <div className="justify-align sr-only-focusable">
                                    <span className="">Scope&#10240;</span>
                                    <span className="Directional Element Green"><img src={`${process.env.PUBLIC_URL}/images/ArrowGreen.png`} alt="Green Arrow" style={{ width: "10px", height: "10px" }} /></span>
                                    {/* <span className="Directional Element Green">→</span> */}
                                </div>
                                <span className="Change">No Change</span>
                            </div>
                        </td>
                        <td data-title="Quality">
                            <div className="aui-td flex-column align-items-start justify-content-center">
                                <div className="justify-align">
                                    <span className="">Quality&#10240;</span>
                                    {/* <span className="Directional Element Red">&#8593;</span> */}
                                    <span className="Directional Element Red"> <img
                                        src={`${process.env.PUBLIC_URL}/images/ArrowRedUp.png`}
                                        alt="Red Arrow Direction Up"
                                        style={{ width: "10px", height: "10px" }}
                                    /></span>
                                </div>
                                <span className="Change Red">
                                    Changes&#10240;<strong>20%</strong>
                                </span>
                            </div>
                        </td>
                        <td data-title="Schedule">
                            <div className="aui-td flex-column align-items-start justify-content-center">
                                <div className="d-flex flex-column justify-align">
                                    <div>
                                        <span className="">Schedule&#10240;</span>
                                        <span className="Directional Element Green"><img src={`${process.env.PUBLIC_URL}/images/ArrowGreen.png`} alt="ArrowGreen" style={{ width: "10px", height: "10px" }} /></span>
                                    </div>
                                    <span className="Change">No Change</span>
                                </div>
                            </div>
                        </td>
                        <td data-title="Budget">
                            <div className="aui-td flex-column align-items-start justify-content-center">
                                <div className="justify-align">
                                    <span className="">Budget&#10240;</span>
                                    <span className="Directional Element Green"> <img
                                        src={`${process.env.PUBLIC_URL}/images/ArrowRedUp.png`}
                                        alt="Red Arrow Direction Up"
                                        style={{ width: "10px", height: "10px" }}
                                    /></span>
                                </div>
                                <span className="Change Red word-nowrap">
                                    Changes&#10240;<strong>20%</strong>
                                </span>
                            </div>
                        </td>
                        <td data-title="Overall Health">
                            <div className="aui-td flex-column align-items-start justify-content-center">
                                <div className="d-flex flex-row justify-content-between">
                                    <div className="d-flex flex-column">
                                        <span className="Change Green">
                                            <strong>Overall Health&#10240;</strong>{" "}
                                        </span>
                                        <span className="week-content">1/11/2021 to 6/11/2021</span>
                                    </div>
                                    <div className="progress--circle progress--5 ">
                                        <span className="d-flex text-nowrap align-items-center h-100"> <img src={`${process.env.PUBLIC_URL}/images/RedProgress.png`} alt="Progresscircle" style={{ width: "40px", height: "40px" }} /></span>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr className="aui-table-status-grey">
                        <td className="title" data-title="Project Title">
                            <div className="aui-td justify-align-title">
                                <a href="/projectlist/projectname">Claps</a>
                            </div>
                        </td>
                        <td data-title="Scope">
                            <div className="aui-td flex-column align-items-start justify-content-center">
                                <div className="justify-align">
                                    <span className="">Scope&#10240;</span>
                                    <span className="Directional Element Green"><img src={`${process.env.PUBLIC_URL}/images/ArrowGreen.png`} alt="ArrowGreen" style={{ width: "10px", height: "10px" }} /></span>
                                </div>
                                <span className="Change">No Change</span>
                            </div>
                        </td>
                        <td data-title="Quality">
                            <div className="aui-td flex-column align-items-start justify-content-center">
                                <div className="justify-align ">
                                    <span className="">Quality&#10240;</span>
                                    <span className="Directional Element Green"> <img
                                        src={`${process.env.PUBLIC_URL}/images/ArrowRedUp.png`}
                                        alt="Red Arrow Direction Up"
                                        style={{ width: "10px", height: "10px" }}
                                    /></span>
                                </div>
                                <span className="Change Red">
                                    Changes&#10240;<strong>20%</strong>
                                </span>
                            </div>
                        </td>
                        <td data-title="Schedule">
                            <div className="aui-td flex-column align-items-start justify-content-center">
                                <div className="d-flex flex-column justify-align">
                                    <div>
                                        <span className="">Schedule&#10240;</span>
                                        <span className="Directional Element Green"><img src={`${process.env.PUBLIC_URL}/images/ArrowRed.png`} alt="Red Arrow" style={{ width: "10px", height: "10px" }} /></span>
                                    </div>
                                    <span
                                        className="Change Red"
                                        style={{ wordWrap: "break-word" }}
                                    >
                                        Rescheduled to Jan 2022
                                    </span>
                                </div>
                            </div>
                        </td>
                        <td data-title="Budget">
                            <div className="aui-td flex-column align-items-start justify-content-center">
                                <div className="justify-align">
                                    <span className="">Budget&#10240;</span>
                                    <span className="Directional Element Green"><img src={`${process.env.PUBLIC_URL}/images/ArrowRed.png`} alt="Red Arrow" style={{ width: "10px", height: "10px" }} /></span>
                                </div>
                                <span className="Change Red">
                                    Changes&#10240;<strong>20%</strong>
                                </span>
                            </div>
                        </td>
                        <td data-title="Overall Health">
                            <div className="aui-td flex-column align-items-start justify-content-center">
                                <div className="d-flex flex-row justify-content-between">
                                    <div className="d-flex flex-column">
                                        <span className="Change Red">
                                            <strong>Overall Health&#10240;</strong>{" "}
                                        </span>
                                        <span className="week-content">1/11/2021 to 6/11/2021</span>
                                    </div>
                                    <div className="progress--circle progress--5 ">
                                        {/* <span className="d-flex text-nowrap align-items-center h-100"> 5%</span> */}
                                        <span className="d-flex text-nowrap align-items-center h-100"> <img src={`${process.env.PUBLIC_URL}/images/GreenProgress.png`} alt="Progresscircle" style={{ width: "40px", height: "40px" }} /></span>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </Table>
    );
}

export default Tables;
